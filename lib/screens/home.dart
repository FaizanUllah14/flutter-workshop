import 'package:flutter/material.dart';
import 'package:workshop_prep/widgets/home/home-large.dart';
import 'package:workshop_prep/widgets/home/home-small.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      return constraints.maxWidth > 600 ? HomeScreenLarge() : HomeScreenSmall();
    });
  }
}
