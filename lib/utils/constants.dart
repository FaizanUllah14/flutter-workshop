import 'package:flutter/material.dart';
import 'package:workshop_prep/models/menu-item.dart';

class Constant {
  static List<MenuItem> MENU_ITEMS = [
    MenuItem(icon: Icons.notifications, title: 'Chats'),
    MenuItem(icon: Icons.settings, title: 'Settings'),
    MenuItem(icon: Icons.info, title: 'About'),
  ];

  static List<Color> CHAT_HEAD_COLORS = [
    Color(0xffF8A6AE),
    Color(0xffB5CAF9),
    Color(0xffFBE797),
  ];
}
