import 'package:flutter/material.dart';
import 'package:workshop_prep/widgets/chat/list.dart';

class ChatContainer extends StatelessWidget {
  const ChatContainer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top, right: 20),
            height: 30 + MediaQuery.of(context).padding.top,
            decoration: BoxDecoration(
                color: Colors.grey[50],
                border: Border(bottom: BorderSide(color: Colors.grey[200]))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  'Chats by:',
                  style: TextStyle(
                      inherit: false,
                      color: Colors.grey,
                      fontSize: 18,
                      // fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.w600),
                ),
                Container(
                    margin: EdgeInsets.only(left: 5),
                    height: 30,
                    padding: EdgeInsets.only(bottom: 4, top: 4),
                    child: Image.asset(
                      'assets/logo-color.png',
                      fit: BoxFit.contain,
                    ))
              ],
            ),
          ),
          Expanded(child: ChatList()),
        ],
      ),
    );
  }
}
