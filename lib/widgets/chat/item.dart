import 'dart:math';

import 'package:flutter/material.dart';
import 'package:workshop_prep/utils/constants.dart';

class ChatItem extends StatelessWidget {
  const ChatItem({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _random = new Random();
    int next(int min, int max) => min + _random.nextInt(max - min);
    Color chatColor =
        Constant.CHAT_HEAD_COLORS[next(0, Constant.CHAT_HEAD_COLORS.length)];

    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return Container(
            padding: EdgeInsets.all(20),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 1,
                  child: CircleAvatar(
                    radius: 35,
                    backgroundColor: chatColor,
                  ),
                ),
                Expanded(
                  flex: 9,
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: 10, left: 10),
                        height: 15,
                        width: constraints.maxWidth,
                        decoration: BoxDecoration(
                            color: Colors.grey[300],
                            borderRadius:
                                BorderRadius.all(Radius.circular(12))),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        height: MediaQuery.of(context).size.height * .09,
                        width: constraints.maxWidth,
                        decoration: BoxDecoration(
                            color: chatColor,
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(12),
                              bottomLeft: Radius.circular(12),
                              bottomRight: Radius.circular(12),
                            )),
                      ),
                    ],
                  ),
                ),
              ],
            ));
      },
    );
  }
}
