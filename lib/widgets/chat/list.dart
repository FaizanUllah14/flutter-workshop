import 'package:flutter/material.dart';
import 'package:workshop_prep/widgets/chat/item.dart';

class ChatList extends StatelessWidget {
  const ChatList({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: 50,
      separatorBuilder: (BuildContext context, int index) {
        return Container(
            decoration: BoxDecoration(
                border:
                    Border(bottom: BorderSide(width: 1, color: Colors.grey))));
      },
      itemBuilder: (BuildContext context, int index) {
        return ChatItem();
      },
    );
  }
}
