import 'package:flutter/material.dart';
import 'package:workshop_prep/widgets/drawer/list.dart';

class DrawerContainer extends StatelessWidget {
  const DrawerContainer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.blueAccent[100],
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) {
                  return Center(
                    child: SizedBox(
                        width: constraints.maxHeight * .8,
                        child: Image.asset(
                          'assets/logo-white.png',
                          fit: BoxFit.contain,
                        )),
                  );
                },
              ),
            ),
            Expanded(
              flex: 4,
              child: DrawerList(),
            )
          ],
        ));
  }
}
