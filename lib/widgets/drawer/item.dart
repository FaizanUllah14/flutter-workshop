import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:workshop_prep/models/menu-item.dart';

class DrawerItem extends StatelessWidget {
  final MenuItem item;

  const DrawerItem({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return Container(
            padding: EdgeInsets.all(20),
            child: Row(children: [
              Icon(
                item.icon,
                color: Colors.yellow[200],
              ),
              Container(
                margin: EdgeInsets.only(left: 20),
                width: constraints.maxWidth * .6,
                child: Text(
                  item.title,
                  style: TextStyle(
                      inherit: false,
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ]));
      },
    );
  }
}
