import 'package:flutter/material.dart';
import 'package:workshop_prep/utils/constants.dart';
import 'package:workshop_prep/widgets/drawer/item.dart';

class DrawerList extends StatelessWidget {
  const DrawerList({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: Constant.MENU_ITEMS.length,
      separatorBuilder: (BuildContext context, int index) {
        return Container(
          decoration: BoxDecoration(
              color: Colors.blueAccent,
              border: Border(bottom: BorderSide(width: 1, color: Colors.grey))),
        );
      },
      itemBuilder: (BuildContext context, int index) {
        return DrawerItem(item: Constant.MENU_ITEMS[index]);
      },
    );
  }
}
