import 'package:flutter/material.dart';
import 'package:workshop_prep/widgets/chat/container.dart';
import 'package:workshop_prep/widgets/drawer/container.dart';
import 'package:workshop_prep/widgets/drawer/list.dart';

class HomeScreenLarge extends StatefulWidget {
  HomeScreenLarge({Key key}) : super(key: key);

  @override
  _HomeScreenLargeState createState() => _HomeScreenLargeState();
}

class _HomeScreenLargeState extends State<HomeScreenLarge> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Expanded(
            child: Row(
              children: [
                Expanded(flex: 1, child: DrawerContainer()),
                Expanded(flex: 2, child: ChatContainer()),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
