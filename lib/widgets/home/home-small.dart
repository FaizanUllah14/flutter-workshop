import 'package:flutter/material.dart';
import 'package:workshop_prep/widgets/chat/container.dart';
import 'package:workshop_prep/widgets/drawer/container.dart';

class HomeScreenSmall extends StatefulWidget {
  HomeScreenSmall({Key key}) : super(key: key);

  @override
  _HomeScreenSmallState createState() => _HomeScreenSmallState();
}

class _HomeScreenSmallState extends State<HomeScreenSmall> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          padding: EdgeInsets.only(bottom: AppBar().preferredSize.height * .2),
          height: AppBar().preferredSize.height * .8,
          child: Image.asset(
            'assets/logo-white.png',
            fit: BoxFit.contain,
          ),
        ),
        backgroundColor: Colors.blueAccent[100],
      ),
      body: ChatContainer(),
      drawer: Drawer(
        child: DrawerContainer(),
      ),
    );
  }
}
